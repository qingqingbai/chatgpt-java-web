 ### 使用Java语言作为后端对接 chatgpt，使用简单，并且有可以免费chatgpt3.5,没有套路
 ### 后端架构：mysql,springBoot 部署简单维护也简单
 ###蓝猫AI后端服务指引
### 效果可以体验 ： http://www.chosen1.xyz/
### 对应前端地址： https://gitee.com/lixinjiuhao/chatgpt-web-java
 
 ##服务器中运行脚本： 
    java -jar xxx.jar --spring.profiles.active=dev
 ## 本地运行：
     线上环境
     vm option添加：-Dspring.profiles.active=prod
     nohup java -jar -Xmx512m -Xms512m -XX:MaxPermSize=256m -XX:PermSize=128m -XX:MetaspaceSize=256M  -XX:MaxMetaspaceSize=256M  -XX:+UseParallelGC -XX:+PrintGCDetails -XX:+PrintGCDateStamps -Xloggc:/var/log/myapp/gc.log  blue-cat-0.0.8-SNAPSHOT.jar --spring.profiles.active=prod 
     本地环境
     vm option添加：-Dspring.profiles.active=dev

## maven打包命令
    mvn clean install -U -Dmaven.test.skip=true

## 后端定制化返回markdowm格式给前端进行图片展示：
![提示词]("url")

##上效果图
![pc端的聊天效果图](https://gitee.com/lixinjiuhao/chatgpt-java-web/raw/master/image/pc-chat-black.jpg)
![pc端的黑色背景聊天图](image/pc-chat-black.jpg)
![pc端的黑白色背景聊天图](image/image/pc-chat-white.jpg)
![pc端prompt工具图](image/pc-tool.jpg)
![手机端的黑色背景聊天图](image/phone-chat-black.jpg)
![手机端的白色背景聊天图](image/phone-chat-white.jpg)
![手机端的prompt工具图](image/phton-tool.jpg)
